<?php
include '_lib.php';
?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <head>
  	<title>AV-Tool</title>
    <link rel="stylesheet" href="/av-tool/css/style.css" type="text/css" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  </head>
<?php 

$LOGO='<ul class="arrowunderline">
			<li><a href="?">Aktueller Wahlgang</a></li>
			<li><a href="?all=true">Alle Kandidaten</a></li>
			<li><a href="?load=wahl">Wahlen</a></li>
'.($_SESSION["admin"]?'
			<li><a href="?load=edit">Kanidate be.</a></li>
			<li><a href="?load=admin">Admin</a></li>
			<li><a href="?load=klist">Kanidatenliste</a></li>
			<li><a href="?load=edit_wahl">Wahl be.</a></li>
':'	
			<li><a href="?load=edit">Mein Seite bearbeiten</a></li>
			<li><a href="?load=admin">Admin Login</a></li>
			').'
		</ul>';

if ($_GET["load"]){
	if (file_exists("view/".$_GET["load"].".php")){
		include_once "view/".$_GET["load"].".php";
	}elseif (file_exists("text/".$_GET["load"].".php")){
		$_GET["text"] = $_GET["load"];
		include_once "view/text.php";
	}else{
		$_GET["text"] = "404";
		include_once "view/text.php";
	}
}else{
	$_GET["text"] = "home";
	include_once "view/text.php";
}
?>
</html>
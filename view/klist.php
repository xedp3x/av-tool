<?php if (!$_SESSION["admin"]){?>
	Zugang verweigert!<br />
	<a href="?">Login</a>
<?php die();}?>

<body>
	<?=$LOGO; ?>
	<h1>Kanidatenliste bearbeiten (Aktueller Wahlgang)</h1>
	<form method="post" style="float: left;" action="?load=klist">
		<?php if($_POST["liste"])
			echo "Nummer: <big style='font-size: 300%;'> ".KANDIDAT_AddListe($_POST["liste"])."</big><br />";
			 ?>
		Kandidaten ID:<input name=liste autofocus=autofocus /><input type=submit value="Auf die Liste Setzen!" />
	</form>
	<form method="post"  action="?load=klist" onsubmit='return confirm("Bist du dier Ganz sicher das du das willst?")' style="float: right;">
		<?php 
			if ($_POST["remove_all"]) KANDIDAT_clearlist();
		?>
		<input type=submit name="remove_all" value="Liste leeren" /> 
	</form>
	
	<h2 style="clear:both;"></h2>
	<table>
    <?php 
    
    if ($_GET["aktion"] == "rem") KANDIDAT_RemListe($_GET["id"]);
    
    $ks= KANDIDAT_all(false);
    $set = $ks;
    while( list ( $key, $k ) = each ( $ks ) ){
    	$k["data"] = json_decode($k["data"],true);?>
    	<tr>
    		<td><b><?=$k["nummer"]?></b></td>
    		<td style="padding-left: 20px;"><?=htmlentities($k["name"])?></td>
    		<td style="padding-left: 20px;">
    			<a href="?load=show&id=<?=$k["id"]?>">Show</a>
    			<a href="?load=edit&id=<?=$k["id"]?>">Edit</a>
    			<a href="?load=klist&id=<?=$k["id"]?>&aktion=rem">Entfernen</a>
    		</td>
    	</tr>
    <?php } ?>
    </table>
<?php

<?php if (!$_SESSION["admin"]){?>
	Zugang verweigert!<br />
	<a href="?">Login</a>
<?php die();} 

if ($_GET["action"] == "create"){
	$wahl = WAHL_create($_POST["wahl"]);
	WAHL_put($_POST["stimmen"],$wahl);
	header("Location: ?load=edit_wahl&id=".$wahl);
	die();
}

if ($_GET["id"] and $_POST["delete"]){
	WAHL_delete($_GET["id"]);
	header("Location: ?load=edit_wahl");
	die();
}

function WahlZeile($k){
	global $counter;
	$counter++;?>
	<tr>
		<td><input name="stimmen[<?=$counter?>][nummer]"	value="<?=$k["nummer"]?>"></td>
		<td><input name="stimmen[<?=$counter?>][stimmen]"	value="<?=$k["stimmen"]?>"></td>
		<td><input name="stimmen[<?=$counter?>][sieg]"		value="<?=$k["sieg"]?>"></td>
		<td><input name="stimmen[<?=$counter?>][kandidat]"	value="<?=$k["kandidat"]?>"></td>
		<td>
			<?php if ($k["id"] <> 0){?>
				<input type="hidden" name="stimmen[<?=$counter?>][id]"value="<?=$k["id"]?>">
			<?php }?>
			<?=(($k["name"]==NULL and isset($k["kandidat"]))?"- nicht Gefunden -":htmlentities($k["name"]))?>
		</td>
	</tr>
	<?php
}


?>
<body>
	<?=$LOGO;?>
	
<? if (!$_GET["id"]){?>
<h1>Wahlgang bearbeiten</h1>
<form>
	<input type=hidden name=load value="edit_wahl" />
	<input type=submit name=new value="Neuer Wahlgang" />
	<input type=submit name=import value="Neuer Wahlgang mit Kanidatenimport" />
</form>
<?php if ($_GET["import"]){
	$ks= KANDIDAT_all($_GET["all"]);?>
	<h2></h2>
<form action="?load=edit_wahl&action=create" method="post">
	Name des Wahlgangs: <input name="wahl[gang]"/>
	<table>
		<tr>
			<th>Nummer</th>
			<th>Stimmen</th>
			<th>Kommentar</th>
			<th>K-ID (0= Löschen)</th>
			<th>Name</th>
		</tr>
	<?php while( list ( $key, $k ) = each ( $ks ) ){
	$k["kandidat"]= $k["id"];
	$k["id"]=0;
	WahlZeile($k); } 
	WahlZeile(array());?>
	</table>
	<input type=submit value=Speichern>
</form>
	
<?php } else { 
	echo "<ul>";
	$set = WAHL_list();
	foreach ($set as $wahl){?>
		<li><a href="?load=edit_wahl&id=<?=$wahl["id"]?>"><?=$wahl["gang"]?></a></li>
	<?php }
echo "</ul>";
}
}else{
	if ($_POST){
		WAHL_set($_GET["id"],$_POST["wahl"]);
		WAHL_put($_POST["stimmen"],$_GET["id"]);
	}
	$wahl= WAHL_get($_GET["id"]);
	$st = WAHL_stimmen($_GET["id"]);?>
	<h1>Wahlgang ID:<?=$wahl["id"]?></h1>
	<div style="float: right;">
		<form method="post" onsubmit='return confirm("Wirklich löschen?")'>
			<input type=submit name="delete" value="Wahlgang löschen">
		</form>
	</div>
	<form method="post">
	Name des Wahlgangs: <input name="wahl[gang]" value="<?=$wahl["gang"]?>"/>
	<h2 style="clear: both;"></h2>
	<table>
		<tr>
			<th>Nummer</th>
			<th>Stimmen</th>
			<th>Kommentar</th>
			<th>K-ID (0= Löschen)</th>
			<th>Name</th>
		</tr>
	<?php while( list ( $key, $k ) = each ( $st ) ){
	WahlZeile($k); } 
	WahlZeile(array());?>
	</table>
	<input type=submit value=Speichern>
</form>
<?php }
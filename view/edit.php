<body>
	<?=$LOGO; ?>
    <h1 id="title">Profiel Bearbeiten</h1>
    <?php 
    
    if ((($_GET["pass"]==substr(md5($_GET["id"].$config["edit_key"]),0,8)) and $config["self_edit"]) or ($_SESSION['admin'])){
    	$_SESSION['id']=$_GET["id"];
	    if ($_POST){
	    	$set = $_POST["data"]["link"];
	    	while( list ( $key, $val ) = each ( $set ) )
	    		if ($val["data"] == "") unset($_POST["data"]["link"][$key]);
	    	if($_FILES["image"]["name"]){
	    		move_uploaded_file($_FILES['image']['tmp_name'], "../img/".time().$_FILES["image"]["name"]);
	    		KANDIDAT_SetImage($_GET["id"],"http://".$config["hostname"]."/img/".time().$_FILES["image"]["name"]);
	    	}
	    	KANDIDAT_update($_GET["id"], $_POST["name"], $_POST["nummer"], $_POST["data"]);
	    }
	    
	    $k = KANDIDAT_get($_GET["id"]);  
	    if (!$_GET["id"]){  
	    ?>
	    <form>
	    	Kanidaten-ID: <input name=id autofocus="autofocus"/>
	    	<input type=submit value=Weiter>
	    	<input name=load value=edit type=hidden>
	    </form>
	    <?php }else {?>
	    <form method="post" enctype="multipart/form-data">
	    <div style="float: left;">
	    <h2>Für den Beamer</h2>
	    <table>
	    	<?php if (!$_GET["pass"]){?><tr><td>Wahl-Nummer:</td><td><input name=nummer value="<?=$k["nummer"]?>" /> (0 = Nicht aktueller Wahlgang)</td></tr><?php }?>
	    	<tr><td>Name:</td><td><input name=name value="<?=$k["name"]?>" /></td></tr>
	    	<tr><td>Kurztext:<br /> (140 Zeichen)</td><td><textarea rows="3" cols="50" name=data[text]><?=$k["data"]["text"]?></textarea></td></tr>
	    	<?php $set = $k["data"];
	    	while( list ( $key, $val ) = each ( $set["link"] ) ){?>
	    		<tr><td><?=array2selectKey($TYPS,"data[link][$key][type]",'','',$val["type"])?></td><td><input name="data[link][<?=$key?>][data]" value="<?=$val["data"]?>" /></td></tr>
	    	<?php }
	    	$new= time();
	    	?>
	    	<tr><td><?=array2selectKey($TYPS,"data[link][$new][type]",'','','')?></td><td><input name="data[link][<?=$new?>][data]" value="" /></td></tr>    	
	    </table>
	    	<h2>Algemeine Infos</h2>
	    	Kanidiere für (z.B.: 1-3,5,7,9-x)	<br /><textarea rows="5"  cols="70" name=data[kanidat]><?=$k["data"]["kanidat"]?></textarea><br />
	    	Langtext: (Vereinfachte Wiki-Syntax) <br /><textarea rows="20" cols="70" name=data[long]><?=$k["data"]["long"]?></textarea><br />
	    	<input type=submit />	 
	    </div><div style="float: right;">
	 	<img src="<?=$k["image"]?>" /><br />
	 	Upload: <input name="image" type="file" /> <input type=submit />
	 	</div>
	    
	    </form>
	<?php } } else {
		if ($config["self_edit"]){ ?>
		<form>
			Login Daten bekommst du bei der Akkredetierung.<br /><br />
			<?php if ($_GET["id"]){?>
				<div style="background-color: red; text-align: center; width: 400px;"><br />Die Daten sind leider Falsch<br /><br /></div><br />
			<?php }?>
			<table>
				<tr><td>Nummer:</td><td><input name=id value="<?=$_SESSION['id']?>" /></td></tr>
				<tr><td>Password:</td><td><input name=pass /></td></tr>
			</table>
			<input type=submit value=Weiter />
			<input type=hidden name=load value=edit />		
		</form>
	<?php }else {?>
		<div style="background-color: red; text-align: center;"><br />Das ändern von Daten ist nur noch durch die Versamlungsleitung möglich...<br /><br /></div>
	<?php }}?>
</body>
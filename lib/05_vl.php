<?php
function VL_put($id,$data){
	global $config;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1:8001/agenda/$id/save");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: PUT'));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	$ce = curl_exec($ch);
	curl_close($ch);
	return $ce;
}
<?php

function WAHL_list(){
	return SQL_select_as_array('wahl');
}

function WAHL_get($id){
	return SQL_select_one('wahl',"id = ".($id+0));
}
function WAHL_stimmen($id,$order="stimmen.nummer"){
	return SQL_query_as_array("SELECT stimmen . * , kandidat . * , stimmen.id AS id, stimmen.nummer AS nummer
FROM `stimmen` LEFT JOIN kandidat on (stimmen.kandidat = kandidat.id) where stimmen.wahl = ".($id+0).' order by '.$order);
}

function WAHL_submit($id){
	global $config;
	$w = WAHL_get($id);
	
	$wn= "<table style='width: 100%'><tr>
	<th>Nummer</th>
	<th style='text-align: center;'>Name</th>
	<th style='text-align: right;'>Stimmen</th>
	<th></th></tr>";
	$sn= WAHL_stimmen($id,"stimmen.nummer");
	while( list ( $key, $k ) = each ( $sn ) )
		$wn .="<tr>
		<td style='text-align: center;'>{$k["nummer"]}</td>
		<td>".htmlentities($k["name"])."</td>
				<td style='text-align: right;'>{$k["stimmen"]}</td>
				<td>{$k["sieg"]}</td>
				</tr>";
	$wn .= "</table>";
	
	$ws= "<table style='width: 100%'><tr>
			<th>Nummer</th>
			<th style='text-align: center;'>Name</th>
			<th style='text-align: right;'>Stimmen</th>
			<th></th></tr>";
	$ss= WAHL_stimmen($id,"stimmen DESC");
	while( list ( $key, $k ) = each ( $ss ) )
		$ws .="<tr>
			<td style='text-align: center;'>{$k["nummer"]}</td>
			<td>".htmlentities($k["name"])."</td>
			<td style='text-align: right;'>{$k["stimmen"]}</td>
			<td>{$k["sieg"]}</td>
		</tr>";
	$ws .= "</table>";
	
	return VL_put("wn$id",http_build_query(array(
			"api"           => $config["vl_api"],
			"slide[hidden]" => "true",
			"slide[isdone]" => "false",
			"slide[type]"   => "html",
			"slide[hide]"   => "false",
			"slide[title]"  => $w["gang"],
			"slide[html]"   => $wn
	)))." - ".
	VL_put("ws$id",http_build_query(array(
			"api"           => $config["vl_api"],
			"slide[hidden]" => "true",
			"slide[isdone]" => "false",
			"slide[type]"   => "html",
			"slide[hide]"   => "false",
			"slide[title]"  => $w["gang"]." (sortiert)",
			"slide[html]"   => $ws
	)));
}

function WAHL_set($id,$data){
	SQL_update('wahl','id = '.($id+0), $data);
	WAHL_submit($id);
}

function WAHL_create($data){
	return SQL_insert('wahl', $data);
}

function WAHL_put($stimmen,$wahl=false){
	foreach ($stimmen as $stimme){
		if ($wahl) $stimme["wahl"] = $wahl;
		if (($stimme["kandidat"]+0)<>0){
			SQL_insert_update('stimmen', $stimme);
		}elseif(isset($stimme["id"])){
			SQL_delete('stimmen', "id = ".($stimme["id"]+0));
		}
	}
	if ($wahl)
		WAHL_submit($wahl);
}

function WAHL_delete($id){
	SQL_delete('wahl','id = '.($id+0));
}
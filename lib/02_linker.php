<?php
$TYPS= array(
		"wiki" 		=> "http://",
		"twitter"	=> "http://",
		"google+"	=> "http://",
		"blog"		=> "http://",
		"facebook"	=> "http://",
		"url"		=> "http://"
);

function GenLink($type, $data){
	switch ($type){
		case "wiki":	return "<a href='http://wiki.piratenpartei.de/$data'>$data</a>";
		case "twitter":	return "<a href='http://twitter.com/$data'>@$data</a>";
		case "google+":	return "<a href='http://twitter.com/$data'>Google+</a>";
		case "blog":	return "<a href='http://$data'>$data</a>";
		case "facebook":return "<a href='http://twitter.com/$data'>Facebook</a>";
		case "url":		return "<a href='$data'>$data</a>";
	}
}

function GenText($type, $data){
	switch ($type){
		case "wiki":	return "Wiki: $data";
		case "twitter":	return "@$data";
		case "google+":	return "Auf Google+";
		case "blog":	return "$data";
		case "facebook":return "Auf Facebook";
		case "url":		return "$data";
	}
}
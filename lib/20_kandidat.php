<?php

function KANDIDAT_all($all){
	return SQL_select_as_array('kandidat',($all?false:"nummer > 0"),"*",($all?"name":"nummer"));
}

function KANDIDAT_get($id){
	$data = SQL_select_one('kandidat',"id = ".($id+0));
	$data["data"] = json_decode($data["data"],true);
	return $data;
}

function KANDIDAT_GenSlide($data,$k){
	$back = '<!-- 

 ! ! ! ! Nicht Editieren ! ! ! !

--><div style="float: left; width: 68%;">
    	<div style="font-size: 150%">'.htmlentities(utf8_decode($data["text"])).'</div>
    	<br /><br />
    	';
    $set = $data;
    while( list ( $key, $val ) = each ( $set["link"] ) )
    	$back .= GenText($val["type"],$val["data"]).'<br />';
    $back .= '</div>
    <div style="float: right; width: 30%;">
    	<center><b style="padding-left: 50px; padding-right: 50px; background: #000000; border-radius: 10px; color: #FFFFFF; font-size: 300%; position:relative; top: -85px;">'.$k["nummer"].'</b></center>
    	<img src="'.$k["image"].'" style="width:100%; position:relative; top: -70px;"/>
    </div>';
    return $back;
}

function KANDIDAT_submit_list(){
	global $config;
	$set = KANDIDAT_all(false);
	while( list ( $key, $k ) = each ( $set ) ){
		$liste .= "<li>{$k['nummer']} - {$k['name']}</li>";
	}
	
	return VL_put("liste",http_build_query(array(
			"api"           => $config["vl_api"],
			"slide[hidden]" => "true",
			"slide[isdone]" => "false",
			"slide[type]"   => "html",
			"slide[hide]"   => "false",
			"slide[title]"  => "Kandidatenliste",
			"slide[html]"   => "<ul>$liste</ul>"
	)));
}

function KANDIDAT_submit($id,$update_list = true){
	global $config;
	$k = KANDIDAT_get($id);
	
	if ($k["nummer"] == 0){
		$data = http_build_query(array(
				"api"           => $config["vl_api"]
		));
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1:8001/agenda/k$id/delete");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($tuCurl, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$out = curl_exec($ch);
		curl_close($ch);
	}else{
		$out = VL_put("k$id", http_build_query(array(
				"api"           => $config["vl_api"],
				"slide[hidden]" => "true",
				"slide[isdone]" => "false",
				"slide[type]"   => "html",
				"slide[hide]"   => "false",
				"slide[title]"  => $k["nummer"]." - ".$k["name"],
				"slide[html]"   => KANDIDAT_GenSlide($k["data"],$k)
		)));
	}
	
	if ($update_list)
		KANDIDAT_submit_list();
	return $out;
}

function KANDIDAT_clearlist(){
	global $config;
	$set = KANDIDAT_all(false);
	while( list ( $key, $k ) = each ( $set ) ){
		$data = http_build_query(array(
				"api"           => $config["vl_api"]
		));
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1:8001/agenda/k{$k["id"]}/delete");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($tuCurl, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$ce = curl_exec($ch);
		curl_close($ch);
	}
	SQL_query("update kandidat set nummer = 0");
	
	return VL_put("liste",http_build_query(array(
			"api"           => $config["vl_api"],
			"slide[hidden]" => "true",
			"slide[isdone]" => "false",
			"slide[type]"   => "html",
			"slide[hide]"   => "false",
			"slide[title]"  => "Kandidatenliste",
			"slide[html]"   => ""
	)));
}

function KANDIDAT_update($id, $name, $nummer, $data){
	$back= SQL_insert_update('kandidat', array(
			"id"	=> $id+0,
			"data" 	=> json_encode($data),
			"name"	=> $name,
			"nummer"=> $nummer
	));
	KANDIDAT_submit($id);
	return $back;
}

function KANDIDAT_AddListe($id){
	$k = KANDIDAT_get($id);
	if ($k["nummer"] > 0)
		return $k["nummer"];
	if ($k["data"] == null)
		return "<span style='background-color: red'>Kandidat existriert nicht!</span>";
	
	$d= SQL_query_as_array("select max(nummer)as nr from kandidat");
	$nr= $d[0]["nr"]+1;
	
	$back= SQL_insert_update('kandidat', array(
			"id"	=> $id+0,
			"nummer"=> $nr
	));
	KANDIDAT_submit($id);
	return $nr;
}

function KANDIDAT_RemListe($id){
	$back= SQL_update('kandidat', "id = ".($id+0), array(
			"nummer"	=> 0
	));
	KANDIDAT_submit($id);
}

function KANDIDAT_SetImage($id,$url){
	$back= SQL_update('kandidat', "id = ".($id+0), array(
			"image"	=> $url
	));
	KANDIDAT_submit($id);
	return $back;
}
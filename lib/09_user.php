<?php

function USER_all(){
	return SQL_select_as_array('user');
}

function USER_ConfigUpdate(){
	global $config;
	$set= USER_all();
	while( list ( $key, $u ) = each ( $set ) ){
		$lv_admin[$u["name"]]=$u["pass"];
	}
	$lv_admin["api"]=$config["vl_api"];
	file_put_contents($config["vl_login"], json_encode($lv_admin));
}

function USER_set($name,$pass){
	SQL_insert_update('user',array(
			"name"	=> $name,
			"pass"	=> $pass
	));
	USER_ConfigUpdate();
}

function USER_delete($name){
	SQL_delete('user', "name = '$name'");
	USER_ConfigUpdate();
}

function USER_login($name,$pass){
	global $_SESSION;
	$u = SQL_select_one('user','name ="'.mysql_real_escape_string($name).'"');
	if (($u["pass"] == $pass) and (isset($u["pass"]))){
		$_SESSION["admin"]= $u;
		return true;
	}else{
		return false;
	}
}
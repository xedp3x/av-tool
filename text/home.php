<body>
	<?=$LOGO; ?>
    <h1 id="title"><?=($_GET["all"]?"Alle":"")?> Kandidaten</h1>
    <table>
    	<tr>
    		<td></td>
    		<td style="width: 50px;"></td>
    		<td></td>
    	</tr>
    <?php 
    $ks= KANDIDAT_all($_GET["all"]);
    $set = $ks;
    while( list ( $key, $k ) = each ( $ks ) ){
    	$k["data"] = json_decode($k["data"],true);?>
    	<tr onclick="window.location = '?load=show&id=<?=$k["id"]?>'">
    		<td><div style="max-height: 160px;">
    			<?php  if ($_GET["all"]){?>
    				<h2><a href="?load=show&id=<?=$k["id"]?>" class="noline"><?=htmlentities($k["name"])?></a></h2>
    			<?php }else{?>
    				<h2><a href="?load=show&id=<?=$k["id"]?>" class="noline"><big><?=$k["nummer"]?></big> - <?=htmlentities($k["name"])?></a></h2>
    			<?php }?>
    			<div><?=nl2br(htmlentities($k["data"]["text"]))?></div>
    		</div></td>
    		<td></td>
    		<td><a href="?load=show&id=<?=$k["id"]?>" class="noline"><img src="<?=$k["image"]?>"  style="height: 150px;"/></a></td>
    	</tr>
    <?php } ?>
    </table>
</body>